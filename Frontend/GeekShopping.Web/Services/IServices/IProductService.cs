using GeekShopping.Web.Models;

namespace GeekShopping.Web.Services.IServices;

public interface IProductService
{
    Task<IEnumerable<ProductModel>> FindAllProducts();
    Task<ProductModel> FindProductById(long id);
    Task<ProductModel> CreateProduct(ProductModel data);
    Task<ProductModel> UpdateProduct(ProductModel data);
    Task<bool> DeleteProductById(long id);
}