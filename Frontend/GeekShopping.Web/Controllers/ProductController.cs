﻿using Microsoft.AspNetCore.Mvc;
using GeekShopping.Web.Models;
using GeekShopping.Web.Services.IServices;

namespace GeekShopping.Web.Controllers;

public class ProductController : Controller
{
    private readonly IProductService _productService;
    private readonly ILogger<ProductController> _logger;

    public ProductController(ILogger<ProductController> logger, IProductService productService)
    {
        _logger = logger;
        _productService = productService ?? throw new ArgumentNullException(nameof(productService));
    }

    public async Task<IActionResult> Index()
    {
        var products = await _productService.FindAllProducts();
        return View(products);
    }

    public IActionResult Create()
    {
        return View();
    }

    [HttpPost]
    public async Task<IActionResult> Create(ProductModel model)
    {
        if (ModelState.IsValid)
        {
            var response = await _productService.CreateProduct(model);
            if (response != null) return RedirectToAction(nameof(Index));
        }
        return View(model);
    }

    public async Task<IActionResult> Update(int id)
    {
        var model = await _productService.FindProductById(id);
        if (model != null) return View(model);
        return NotFound();
    }

    [HttpPost]
    public async Task<IActionResult> Update(ProductModel model)
    {
        if (ModelState.IsValid)
        {
            var response = await _productService.UpdateProduct(model);
            if (response != null) return RedirectToAction(nameof(Index));
        }
        return View(model);
    }

    public async Task<IActionResult> Delete(int id)
    {
        var model = await _productService.FindProductById(id);
        if (model != null) return View(model);
        return NotFound();
    }

    [HttpPost]
    public async Task<IActionResult> Delete(ProductModel model)
    {

        var response = await _productService.DeleteProductById(model.Id);
        if(response) return RedirectToAction(nameof(Index));
        return View(model);
    }

}
